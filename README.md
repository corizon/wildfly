WildFly docker image 

* WildFly 8.2 Final
* Keycloak adapter 1.0.4 Final
* Oracle java 8

to run use: 

```
docker.io run -p8080:8080 -ir limetri/wildfly
```

to change default port offset:

```
docker.io run -e PORT_OFFSET=5   -p 8085:8085 -it limetri/
```

Example to be used as base image:

```
FROM limetri/wildfly

ADD myWar.war /opt/wildfly/standalone/deployments/

```


